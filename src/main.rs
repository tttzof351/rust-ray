mod camera;
mod hitable;
mod hitable_list;
mod ray;
mod sphere;
mod vec3;
mod material;

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

use camera::Camera;
use hitable::{HitRecord, Hitable};
use hitable_list::HitableList;
use ray::Ray;
use sphere::Sphere;
use vec3::Vec3;
use material::rnd_f32;
use material::Material;


const UNIT_VEC: Vec3 = Vec3::new(1.0, 1.0, 1.0);
const COLOR_VEC: Vec3 = Vec3::new(0.5, 0.7, 1.0);

fn color(ray: &Ray, world: &dyn Hitable, depth: u32) -> Vec3 {
    let mut rec: HitRecord = HitRecord::zero();

    if world.hit(ray, 0.001, f32::MAX, &mut rec) {
        
        let mut scattered = Ray::zero();
        let mut attenuation = Vec3::zero();
        
        if depth < 50 && rec.material.scatter(ray, &rec, &mut attenuation, &mut scattered) {
            attenuation * color(
                &scattered,
                world,
                depth + 1
            )
        } else {
            return Vec3::zero()
        }
    } else {
        let unit_direction = ray.direction().unit_vector();
        let t = 0.5 * (unit_direction.y() + 1.0);
        return (1.0 - t) * UNIT_VEC + t * COLOR_VEC;
    }
}

// https://doc.rust-lang.org/rust-by-example/std_misc/file.html
fn main() {    
    let path = Path::new("image.ppm");
    let display = path.display();

    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why),
        Ok(file) => file,
    };

    let multiply: u32 = 400;
    let width: f32 = 2.0;
    let height: f32 = 2.0;

    let nx: u32 = width as u32 * multiply;
    let ny: u32 = height as u32 * multiply;
    let ns: u32 = 10;

    file.write(format!("P3\n{} {}\n255\n", nx, ny).as_bytes())
        .unwrap();

    let mut world = HitableList::with_capacity(2);
    let cam = Camera::new(width, height);

    world
        .list
        .push(Box::new(Sphere::new(
            Vec3::new(0.0, 0.0, -1.0), 
            0.5,
            Material::lambertian(
                Vec3::new(0.8, 0.3, 0.3)
            )
        )));

    world
        .list
        .push(Box::new(Sphere::new(
            Vec3::new(0.0, -100.5, -1.0), 
            100.0,
            Material::lambertian(
                Vec3::new(0.8, 0.8, 0.0)
            )
        )));
    
    world
        .list
        .push(Box::new(Sphere::new(
            Vec3::new(1.0, 0.0, -1.0), 
            0.5,
            Material::metal(
                Vec3::new(0.8, 0.6, 0.2),
                0.0
            )
        )));
              
    world
        .list
        .push(Box::new(Sphere::new(
            Vec3::new(-1.0, 0.0, -1.0), 
            0.5,
            Material::dielectric(1.5)
        )));
              
    for j in (0..ny).rev() {
        for i in 0..nx {
            let mut col: Vec3 = Vec3::zero();
            for _ in 0..ns {
                let u: f32 = (i as f32 + rnd_f32()) / nx as f32;
                let v: f32 = (j as f32 + rnd_f32()) / ny as f32;

                let ray = cam.get_ray(u, v);
                col += &color(&ray, &world, 0);
            }

            col /= ns as f32;
            col = Vec3::new(
                f32::sqrt(col.x()), 
                f32::sqrt(col.y()), 
                f32::sqrt(col.z())
            );
            col *= 255.99;

            let color_line = format!("{} {} {}\n", col.r() as i32, col.g() as i32, col.b() as i32);

            file.write(color_line.as_bytes()).unwrap();
        }
    }
}
