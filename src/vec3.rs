#[derive(Debug, Copy, Clone)]
pub struct Vec3 {
    pub e: [f32; 3],
}

#[allow(dead_code)]
impl Vec3 {
    pub const fn new(e0: f32, e1: f32, e2: f32) -> Self {
        Vec3 { e: [e0, e1, e2] }
    }

    pub const fn zero() -> Self {
        Vec3::new(0.0, 0.0, 0.0)
    }

    pub const fn one() -> Self {
        Vec3::new(1.0, 1.0, 1.0)
    }


    pub const fn copy(&self) -> Self {
        Vec3::new(self.e[0], self.e[1], self.e[2])
    }

    pub fn x(&self) -> f32 {
        self.e[0]
    }
    pub fn y(&self) -> f32 {
        self.e[1]
    }
    pub fn z(&self) -> f32 {
        self.e[2]
    }
    pub fn r(&self) -> f32 {
        self.e[0]
    }
    pub fn g(&self) -> f32 {
        self.e[1]
    }
    pub fn b(&self) -> f32 {
        self.e[2]
    }

    pub fn is_zero(&self) -> bool {
        self.e[0] == 0.0 && self.e[1] == 0.0 && self.e[2] == 0.0
    }

    pub fn length(&self) -> f32 {
        std::primitive::f32::sqrt(self.squared_length())
    }

    pub fn squared_length(&self) -> f32 {
        self.e[0] * self.e[0] + self.e[1] * self.e[1] + self.e[2] * self.e[2]
    }

    pub fn make_unit_vector(&mut self) -> () {
        let k: f32 = 1.0 / self.length();

        self.e[0] *= k;
        self.e[1] *= k;
        self.e[2] *= k;
    }

    pub fn unit_vector(&self) -> Vec3 {
        let mut unit_vec = Vec3::new(self.e[0], self.e[1], self.e[2]);
        unit_vec.make_unit_vector();
        unit_vec
    }

    pub fn dot(&self, right: &Vec3) -> f32 {
        self.e[0] * right.e[0] + self.e[1] * right.e[1] + self.e[2] * right.e[2]
    }

    pub fn cross(&self, right: &Vec3) -> Vec3 {
        let x_cross = self.e[1] * right.e[2] - self.e[2] * right.e[1];
        let y_cross = -(self.e[0] * right.e[2] - self.e[2] * right.e[0]);
        let z_cross = self.e[0] * right.e[1] - self.e[1] * right.e[0];
        Vec3::new(x_cross, y_cross, z_cross)
    }
}

impl std::fmt::Display for Vec3 {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:.1} {:.1} {:.1}", self.e[0], self.e[1], self.e[2])
    }
}

impl std::ops::Add<&Vec3> for &Vec3 {
    type Output = Vec3;

    fn add(self, other: &Vec3) -> Vec3 {
        Vec3::new(
            self.e[0] + other.e[0],
            self.e[1] + other.e[1],
            self.e[2] + other.e[2],
        )
    }
}

impl std::ops::Add<&Vec3> for Vec3 {
    type Output = Vec3;

    fn add(self, other: &Vec3) -> Vec3 {
        Vec3::new(
            self.e[0] + other.e[0],
            self.e[1] + other.e[1],
            self.e[2] + other.e[2],
        )
    }
}

impl std::ops::Add<Vec3> for Vec3 {
    type Output = Vec3;

    fn add(self, other: Vec3) -> Vec3 {
        Vec3::new(
            self.e[0] + other.e[0],
            self.e[1] + other.e[1],
            self.e[2] + other.e[2],
        )
    }
}

impl std::ops::AddAssign<&Vec3> for Vec3 {
    fn add_assign(&mut self, other: &Self) {
        self.e[0] += other.e[0];
        self.e[1] += other.e[1];
        self.e[2] += other.e[2];
    }
}

impl std::ops::Sub<&Vec3> for &Vec3 {
    type Output = Vec3;

    fn sub(self, other: &Vec3) -> Vec3 {
        Vec3::new(
            self.e[0] - other.e[0],
            self.e[1] - other.e[1],
            self.e[2] - other.e[2],
        )
    }
}

impl std::ops::Sub<&Vec3> for Vec3 {
    type Output = Vec3;

    fn sub(self, other: &Vec3) -> Vec3 {
        Vec3::new(
            self.e[0] - other.e[0],
            self.e[1] - other.e[1],
            self.e[2] - other.e[2],
        )
    }
}

impl std::ops::SubAssign<&Vec3> for Vec3 {
    fn sub_assign(&mut self, other: &Self) {
        self.e[0] -= other.e[0];
        self.e[1] -= other.e[1];
        self.e[2] -= other.e[2];
    }
}

impl std::ops::Index<usize> for Vec3 {
    type Output = f32;

    fn index(&self, idx: usize) -> &f32 {
        &self.e[idx]
    }
}

impl std::ops::MulAssign<&Vec3> for Vec3 {
    fn mul_assign(&mut self, other: &Vec3) {
        self.e[0] *= other.e[0];
        self.e[1] *= other.e[1];
        self.e[2] *= other.e[2];
    }
}

impl std::ops::MulAssign<f32> for Vec3 {
    fn mul_assign(&mut self, other: f32) {
        self.e[0] *= other;
        self.e[1] *= other;
        self.e[2] *= other;
    }
}

impl std::ops::Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, other: f32) -> Self::Output {
        Vec3::new(self.e[0] * other, self.e[1] * other, self.e[2] * other)
    }
}

impl std::ops::Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Self::Output {
        Vec3::new(other.e[0] * self, other.e[1] * self, other.e[2] * self)
    }
}

impl std::ops::Mul<f32> for &Vec3 {
    type Output = Vec3;

    fn mul(self, other: f32) -> Self::Output {
        Vec3::new(self.e[0] * other, self.e[1] * other, self.e[2] * other)
    }
}

impl std::ops::Mul<Vec3> for Vec3 {
    type Output = Vec3;

    fn mul(self, other: Vec3) -> Self::Output {
        Vec3::new(self.e[0] * other.e[0], self.e[1] * other.e[1], self.e[2] * other.e[2])
    }
}

impl std::ops::DivAssign<&Vec3> for Vec3 {
    fn div_assign(&mut self, other: &Vec3) {
        self.e[0] /= other.e[0];
        self.e[1] /= other.e[1];
        self.e[2] /= other.e[2];
    }
}

impl std::ops::DivAssign<f32> for Vec3 {
    fn div_assign(&mut self, other: f32) {
        self.e[0] /= other;
        self.e[1] /= other;
        self.e[2] /= other;
    }
}

impl std::ops::Div<f32> for Vec3 {
    type Output = Self;

    fn div(self, other: f32) -> Vec3 {
        Vec3::new(self.e[0] / other, self.e[1] / other, self.e[2] / other)
    }
}

impl std::cmp::PartialEq for Vec3 {
    fn eq(&self, other: &Self) -> bool {
        self.e == other.e
    }
}

#[cfg(test)]
mod test {
    use super::Vec3;

    #[test]
    fn test_xyz() {
        let vec = Vec3::new(1.0, 2.0, 3.0);
        assert_eq!(vec.x(), 1.0);
        assert_eq!(vec.x(), vec.r());

        assert_eq!(vec.y(), 2.0);
        assert_eq!(vec.y(), vec.g());

        assert_eq!(vec.z(), 3.0);
        assert_eq!(vec.z(), vec.b());
    }

    #[test]
    fn test_add() {
        let mut vec_left = Vec3::new(1.0, 2.0, 3.0);
        let vec_right = Vec3::new(4.0, 5.0, 6.0);
        vec_left += &vec_right;
        assert_eq!(vec_left, Vec3::new(5.0, 7.0, 9.0));
    }

    #[test]
    fn test_dot() {
        let vec_left = Vec3::new(1.0, 2.0, 3.0);
        let vec_right = Vec3::new(4.0, 5.0, 6.0);

        let dot1 = vec_left.dot(&vec_right);
        let dot2 = Vec3::dot(&vec_left, &vec_right);
        assert_eq!(dot1, 32.0);
        assert_eq!(dot1, dot2);
    }

    #[test]
    fn test_cross() {
        let x_vec = Vec3::new(1.0, 0.0, 0.0);
        let y_vec = Vec3::new(0.0, 1.0, 0.0);
        let cross_vec = Vec3::cross(&x_vec, &y_vec);

        println!("cross vec: {}", cross_vec);
        assert_eq!(cross_vec, Vec3::new(0.0, 0.0, 1.0));
    }
}
