pub fn random(mut seed: u32) -> f32 {
    seed ^= seed << 17;
    seed ^= seed >> 5;
    seed ^= seed << 13;

    seed as f32 / u32::MAX as f32
}

#[cfg(test)]
mod test {
    use super::random;
    #[test]
    fn test_rand() {
        for i in 0..100 {
            let random_value = random(i);
            println!("value random: {}", random_value);
            assert!(random_value < 1.0);
            assert!(random_value >= 0.0);
        }
    }
}
