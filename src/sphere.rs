use super::hitable::{HitRecord, Hitable};
use super::ray::Ray;
use super::vec3::Vec3;
use super::material::Material;

#[derive(Debug)]
pub struct Sphere {
    center: Vec3,
    radius: f32,
    material: Material
}

impl Sphere {
    pub const fn new(
        center: Vec3, 
        radius: f32,
        material: Material
    ) -> Sphere {
        Sphere {
            center: center,
            radius: radius,
            material: material
        }
    }
}

impl Hitable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32, rec: &mut HitRecord) -> bool {
        rec.material = self.material;
        let oc: Vec3 = ray.origin() - &self.center;

        let a = Vec3::dot(&ray.direction(), &ray.direction());
        let b = Vec3::dot(&oc, &ray.direction());
        let c = Vec3::dot(&oc, &oc) - self.radius * self.radius;

        let discriminant = b * b - a * c;
        if discriminant > 0.0 {
            let temp = (-b - f32::sqrt(b * b - a * c)) / a;
            if temp < t_max && temp > t_min {
                rec.t = temp;
                rec.p = ray.pointer_at_parameter(rec.t);
                rec.normal = (&rec.p - &self.center) / self.radius;
                return true;
            }

            let temp = (-b + f32::sqrt(b * b - a * c)) / a;
            if temp < t_max && temp > t_min {
                rec.t = temp;
                rec.p = ray.pointer_at_parameter(rec.t);
                rec.normal = (&rec.p - &self.center) / self.radius;
                return true;
            }
        }
        return false;
    }
}
