use super::ray::Ray;
use super::vec3::Vec3;

pub struct Camera {
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    origin: Vec3,
}

#[allow(dead_code)]
impl Camera {
    pub fn new(width: f32, height: f32) -> Self {
        let half_width: f32 = width / 2.0;
        let half_hight: f32 = height / 2.0;

        Camera {
            lower_left_corner: Vec3::new(-half_width, -half_hight, -1.0),
            horizontal: Vec3::new(width, 0.0, 0.0),
            vertical: Vec3::new(0.0, height, 0.0),
            origin: Vec3::new(0.0, 0.0, 0.0),
        }
    }

    pub fn get_ray(&self, u: f32, v: f32) -> Ray {
        let direction = &self.lower_left_corner + &(&self.horizontal * u) + &(&self.vertical * v)
            - &self.origin;

        Ray::new(self.origin.copy(), direction)
    }
}

#[cfg(test)]
mod test {
    use super::Camera;
    use super::Vec3;

    #[test]
    fn test_create() {
        let camera = Camera::new(4.0, 2.0);
        assert_eq!(camera.lower_left_corner, Vec3::new(-2.0, -1.0, -1.0));
        assert_eq!(camera.horizontal, Vec3::new(4.0, 0.0, 0.0));
        assert_eq!(camera.vertical, Vec3::new(0.0, 2.0, 0.0));
        assert_eq!(camera.origin, Vec3::new(0.0, 0.0, 0.0));
    }
}
