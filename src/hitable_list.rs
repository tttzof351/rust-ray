use super::hitable::{HitRecord, Hitable};
use super::ray::Ray;

pub struct HitableList {
    pub list: std::vec::Vec<Box<dyn Hitable>>,
}

impl HitableList {
    pub fn with_capacity(capacity: usize) -> Self {
        HitableList {
            list: Vec::with_capacity(capacity),
        }
    }
}

impl Hitable for HitableList {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32, rec: &mut HitRecord) -> bool {
        let mut temp_rec = HitRecord::zero();
        let mut hit_anything = false;
        let mut closet_so_far = t_max;

        for hit in self.list.iter() {
            if hit.hit(ray, t_min, closet_so_far, &mut temp_rec) {
                hit_anything = true;
                closet_so_far = temp_rec.t;

                rec.t = temp_rec.t;
                rec.p = temp_rec.p;
                rec.normal = temp_rec.normal;
                rec.material = temp_rec.material;
            }
        }

        return hit_anything;
    }
}
