use super::ray::Ray;
use super::vec3::Vec3;
use super::hitable::HitRecord;
use rand::*;

pub fn rnd_f32() -> f32 {
    rand::rngs::ThreadRng::default().gen::<f32>()
}

fn random_in_unit_sphere() -> Vec3 {
    loop {
        let p = 2.0 * Vec3::new(
            2.0 * rnd_f32(), 
            2.0 * rnd_f32(), 
            2.0 * rnd_f32()
        ) - &Vec3::one();
        
        if p.squared_length() < 1.0 {
            return p;
        }
    }    
}

fn reflect(v: &Vec3, n: &Vec3) -> Vec3 {
    v - &(n * 2.0 * Vec3::dot(v, n))
}

fn refract(v: &Vec3, n: &Vec3, ni_over_nt: f32, refracted: &mut Vec3) -> bool {
    let uv: Vec3 = v.unit_vector();
    let dt: f32 = Vec3::dot(&uv, n);
    let discriminant: f32 = 1.0 - ni_over_nt * ni_over_nt * (1.0 - dt*dt);
    
    if discriminant > 0.0 {
        let tmp_ref = ni_over_nt * (uv - &(n * dt)) - &(n * f32::sqrt(discriminant));
        refracted.e = tmp_ref.e;
        true
    } else  {
        false
    }
}

#[derive(Debug, Copy, Clone)]
pub enum MaterialType {
    LAMBERIAN,
    METAL,
    DIELECTRIC
}

#[derive(Debug, Copy, Clone)]
pub struct Material {
    pub albedo: Vec3,
    pub fuzz: f32,
    pub ref_idx: f32,
    pub material_type:  MaterialType
}

impl Material {
    pub const fn zero() -> Self {
        Material {
            albedo: Vec3::zero(),
            fuzz: 0.0,
            ref_idx: 0.0,
            material_type: MaterialType::LAMBERIAN
        }
    }

    pub const fn lambertian(albedo: Vec3) -> Self {
        Material { 
            albedo: albedo,
            fuzz: 0.0,
            ref_idx: 0.0,
            material_type: MaterialType::LAMBERIAN
        }
    }    

    pub const fn metal(albedo: Vec3, fuzz: f32) -> Self {
        Material { 
            albedo: albedo,
            fuzz: fuzz,
            ref_idx: 0.0,
            material_type: MaterialType::METAL
        }
    }

    pub const fn dielectric(ri: f32) -> Self {
        Material {
            albedo: Vec3::zero(),
            fuzz: 0.0,
            ref_idx: ri,
            material_type: MaterialType::DIELECTRIC
        }
    }
    
    pub fn scatter(&self, r_in: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool {
        match self.material_type {
            MaterialType::LAMBERIAN => {
                self.scatter_lamberian(r_in, rec, attenuation, scattered)
            },
            MaterialType::METAL => {
                self.scatter_metal(r_in, rec, attenuation, scattered)
            },
            MaterialType::DIELECTRIC => {
                self.scatter_dielectric(r_in, rec, attenuation, scattered)
            }
        }
    }


    fn scatter_metal(&self, r_in: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool {
        let reflected = reflect(&r_in.direction().unit_vector(), &rec.normal);
        
        scattered.origin_vec = rec.p;
        if self.fuzz > 0.0 {
            scattered.direction_vec = reflected + self.fuzz * random_in_unit_sphere();
        } else {
            scattered.direction_vec = reflected;
        }

        attenuation.e = self.albedo.e;

        return Vec3::dot(scattered.direction(), &rec.normal) > 0.0
    }

    fn scatter_lamberian(&self, _: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool {
        let target = rec.p + rec.normal + random_in_unit_sphere();
        scattered.origin_vec = rec.p;
        scattered.direction_vec = target - &rec.p;
        attenuation.e = self.albedo.e;

        true
    }

    fn scatter_dielectric(&self, r_in: &Ray, rec: &HitRecord, attenuation: &mut Vec3, scattered: &mut Ray) -> bool {
        let outward_normal: Vec3;        
        let reflected: Vec3 = reflect(r_in.direction(), &rec.normal);
        let ni_over_nt: f32;
        attenuation.e = Vec3::one().e;
        let mut refracted: Vec3 = Vec3::zero();

        if Vec3::dot(r_in.direction(), &rec.normal) > 0.0 {
            outward_normal = -1.0 * rec.normal;
            ni_over_nt = self.ref_idx;
        } else {
            outward_normal = rec.normal;
            ni_over_nt = 1.0 / self.ref_idx;
        }

        if refract(r_in.direction(), &outward_normal, ni_over_nt, &mut refracted) {
            scattered.origin_vec.e = rec.p.e;
            scattered.direction_vec.e = refracted.e;
            true
        } else {
            scattered.origin_vec.e = rec.p.e;
            scattered.direction_vec.e = reflected.e;
            false
        }        
    }    
}
