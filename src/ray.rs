use super::vec3::Vec3;

#[derive(Debug)]
pub struct Ray {
    pub origin_vec: Vec3,
    pub direction_vec: Vec3,
}

#[allow(dead_code)]
impl Ray {
    pub fn zero() -> Ray {
        Ray {
            origin_vec: Vec3::zero(),
            direction_vec: Vec3::zero()
        }
    }

    pub fn new(or: Vec3, dir: Vec3) -> Ray {
        Ray {
            origin_vec: or,
            direction_vec: dir,
        }
    }

    pub fn origin(&self) -> &Vec3 {
        &self.origin_vec
    }

    pub fn direction(&self) -> &Vec3 {
        &self.direction_vec
    }

    pub fn pointer_at_parameter(&self, t: f32) -> Vec3 {
        &(self.origin_vec) + &(&self.direction_vec * t)
    }
}
